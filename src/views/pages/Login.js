import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./index.css";
import PhoneInput from "react-phone-number-input";
import { getCountryCallingCode } from "react-phone-number-input";
import "react-phone-number-input/style.css";

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Container,
  Col,
  Row,
  CardTitle,
} from "reactstrap";
import axios from "axios";

function Login() {
  const navigate = useNavigate();
  const [value, setValue] = useState();
  const [validationError, setValidationError] = useState({
    phone: false,
    countryCode: false,
  });
  const [countryCode, setCountryCode] = useState();

  React.useEffect(() => {
    document.body.classList.toggle("login-page");
    return function cleanup() {
      document.body.classList.toggle("login-page");
    };
  });

  const sendOtpHandler = async () => {
    if (!countryCode) {
      setValidationError((prevState) => ({
        ...prevState,
        countryCode: true,
      }));
      return;
    }
    if (!value) {
      setValidationError((prevState) => ({
        ...prevState,
        phone: true,
      }));
      return;
    }
    const code = getCountryCallingCode(countryCode);
    try {
      const res = await axios.post(
        "http://16.171.173.51:3000/api/v1/auth/request-otp",
        {
          countryCode: "+91",
          phoneNumber: "8923660954",
          role: "business",
        }
      );
      if (res.status === 200) {
        navigate("/auth/verify-otp");
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="wrapper wrapper-full-page">
      <div className="full-page section-image">
        <div className="login-page">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" lg="4" md="6">
                <Form action="" className="form" method="">
                  <Card className="card-login" style={{ padding: "8px" }}>
                    <CardHeader>
                      <CardHeader>
                        <h3 className="header text-center">Register</h3>
                      </CardHeader>
                    </CardHeader>
                    <CardTitle style={{ textAlign: "center" }}>
                      Please enter phone number to register.
                    </CardTitle>
                    <CardBody>
                      <PhoneInput
                        placeholder="Enter Mobile Number"
                        value={value}
                        onChange={(value) => {
                          setValidationError(false);
                          setValue(value);
                        }}
                        onCountryChange={(country) => {
                          setCountryCode(country);
                        }}
                      />
                      {validationError.phone && (
                        <div style={{ textAlign: "center", marginTop: "4px" }}>
                          <span style={{ color: "red" }}>
                            Phone number is required
                          </span>
                        </div>
                      )}
                      {validationError.countryCode && (
                        <div style={{ textAlign: "center", marginTop: "4px" }}>
                          <span style={{ color: "red" }}>
                            Country code is required
                          </span>
                        </div>
                      )}
                      <br />
                    </CardBody>
                    <CardFooter>
                      <Button
                        block
                        className="btn-round mb-3"
                        color="warning"
                        href="#pablo"
                        onClick={(e) => {
                          e.preventDefault();
                          sendOtpHandler();
                        }}
                      >
                        Send OTP
                      </Button>
                    </CardFooter>
                  </Card>
                </Form>
              </Col>
            </Row>
          </Container>
          <div
            className="full-page-background"
            style={{
              backgroundImage: `url(${require("assets/img/bg/fabio-mangione.jpg")})`,
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default Login;
