import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

const CustomerListing = () => {
  const [customerListingData, setCustomerListingData] = useState();
  const getCustomerListing = async () => {
    const accessToken = Cookies.get("accessToken");
    const res = await axios.get(
      "http://16.171.173.51:3000/api/v1/profile/customers",
      {
        headers: {
          Authorization: accessToken,
        },
      }
    );
    setCustomerListingData(res?.data?.data);
  };

  useEffect(() => {
    getCustomerListing();
  }, []);


  return (
    <div className="content">
      <Row>
        <Col md="12">
          <Card>
            <CardHeader>
              <CardTitle tag="h4">Customer Listing</CardTitle>
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Role</th>
                    <th>Country Code</th>
                    <th>Phone No.</th>
                  </tr>
                </thead>
                <tbody>
                  {customerListingData?.map((data) => {
                    return (
                      <tr>
                        <td>{data?.role}</td>
                        <td>{data?.countryCode}</td>
                        <td>{data?.phoneNumber}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default CustomerListing;
