import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

const BusinessListing = () => {
  const [businessListingData, setBusinessListingData] = useState();
  const getBusinessList = async () => {
    const accessToken = Cookies.get("accessToken");
    const res = await axios.get(
      "http://16.171.173.51:3000/api/v1/admin/businesses?page=1&limit=10",
      {
        headers: {
          Authorization: accessToken,
        },
      }
    );
    setBusinessListingData(res?.data?.data);
  };
  useEffect(() => {
    getBusinessList();
  }, []);
  return (
    <div className="content">
      <Row>
        <Col md="12">
          <Card>
            <CardHeader>
              <CardTitle tag="h4">Business Listing</CardTitle>
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Business Name</th>
                    <th>Is Verified</th>
                    <th>Address</th>
                    <th>Payment Mode</th>
                    <th>Website</th>
                  </tr>
                </thead>
                <tbody>
                  {businessListingData?.map((data) => {
                    return (
                      <tr>
                        <td>{data?.business?.businessName}</td>
                        <td>
                          {data?.business?.isVerified
                            ? "Verified"
                            : "Not Verified"}
                        </td>
                        <td>{data?.business?.address}</td>
                        <td>{data?.business?.paymentMode}</td>
                        <td>{data?.business?.website}</td>
                        <td style={{ cursor: "pointer" }}>View</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default BusinessListing;
