import React, { useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import axios from "axios";
import Cookies from "js-cookie";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";
const CreateCategory = () => {
  const [formData, setFormData] = useState({
    title: "",
    image: "",
  });

  const [validationError, setValidationError] = useState({
    title: false,
    image: false,
  });

  const validator = () => {
    if (!formData.title) {
      setValidationError((prevState) => ({
        ...prevState,
        title: true,
      }));
    }
    if (!formData.image) {
      setValidationError((prevState) => ({
        ...prevState,
        image: true,
      }));
    }
    return !formData.title || !formData.image;
  };

  const createCategoryHandler = async () => {
    if (validator()) {
      return;
    }
    const accessToken = Cookies.get("accessToken");
    const res = await axios.post(
      "http://16.171.173.51:3000/api/v1/admin/categories/store",
      {
        title: formData.title,
        image: formData.image,
      },
      {
        headers: {
          Authorization: accessToken,
          "Content-Type": "multipart/form-data",
        },
      }
    );
    if (res.data.code === 200) {
      toast.success("Category created successfully", {
        duration: 4000,
        position: "top-center",
      });
      setFormData({
        title: "",
        image: "",
      });
      document.getElementById("imageInput").value = "";
    }
  };
  return (
    <div className="content">
      <Row>
        <Col md="6">
          <Card>
            <CardHeader>
              <CardTitle tag="h4">Create New Category</CardTitle>
            </CardHeader>
            <CardBody>
              <Form>
                <label>Title</label>
                <FormGroup>
                  <Input
                    placeholder="Enter Title"
                    type="text"
                    value={formData.title}
                    onChange={(event) => {
                      setFormData((prevState) => ({
                        ...prevState,
                        title: event.target.value,
                      }));
                      setValidationError((prevState) => ({
                        ...prevState,
                        title: false,
                      }));
                    }}
                  />
                  {validationError?.title && (
                    <span style={{ color: "red" }}>
                      Category title is required
                    </span>
                  )}
                </FormGroup>
                <label>Image</label>
                <FormGroup>
                  <Input
                  id="imageInput"
                    placeholder="Select Image"
                    type="file"
                    onChange={(event) => {
                      setFormData((prevState) => ({
                        ...prevState,
                        image: event.target.files[0],
                      }));
                      setValidationError((prevState) => ({
                        ...prevState,
                        image: false,
                      }));
                    }}
                  />
                  {validationError?.image && (
                    <span style={{ color: "red" }}>
                      Category image is required
                    </span>
                  )}
                </FormGroup>
              </Form>
            </CardBody>
            <CardFooter>
              <Button
                className="btn-round"
                color="info"
                type="submit"
                onClick={() => {
                  createCategoryHandler();
                }}
              >
                Submit
              </Button>
            </CardFooter>
          </Card>
        </Col>
      </Row>
      <Toaster />
    </div>
  );
};

export default CreateCategory;
