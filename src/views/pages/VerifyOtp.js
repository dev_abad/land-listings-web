import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import axios from "axios";

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Container,
  Col,
  Row,
  CardTitle,
  Input,
} from "reactstrap";

const VerifyOTP = () => {
  const navigate = useNavigate();
  const [value, setValue] = useState();
  const [validationError, setValidationError] = useState(false);

  React.useEffect(() => {
    document.body.classList.toggle("login-page");
    return function cleanup() {
      document.body.classList.toggle("login-page");
    };
  });

  const verifyOTPHandler = async () => {
    if (!value) {
      setValidationError(true);
      return;
    }
    try {
      const res = await axios.post(
        "http://16.171.173.51:3000/api/v1/auth/verify-otp",
        {
          countryCode: "+91",
          phoneNumber: "8923660954",
          otp: "123456",
        }
      );
      if (res?.data?.accessToken) {
        Cookies.set("accessToken", res?.data?.accessToken);
        Cookies.set("refreshToken", res?.data?.refreshToken);
        navigate("/admin/dashboard");
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="wrapper wrapper-full-page">
      <div className="full-page section-image">
        <div className="login-page">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" lg="4" md="6">
                <Form action="" className="form" method="">
                  <Card className="card-login" style={{ padding: "8px" }}>
                    <CardHeader>
                      <CardHeader>
                        <h3 className="header text-center">Verify OTP</h3>
                      </CardHeader>
                    </CardHeader>
                    <CardTitle style={{ textAlign: "center" }}>
                      Please enter your OTP to successfully register.
                    </CardTitle>
                    <CardBody>
                      <Input
                        placeholder="Enter OTP"
                        value={value}
                        style={{ padding: "8px" }}
                        onChange={(event) => {
                          setValidationError(false);
                          setValue(event.target.value);
                        }}
                      />
                      {validationError && (
                        <span style={{ color: "red" }}>
                          Please enter a valid OTP
                        </span>
                      )}
                      <br />
                    </CardBody>
                    <CardFooter>
                      <Button
                        block
                        className="btn-round mb-3"
                        color="warning"
                        href="#pablo"
                        onClick={(e) => {
                          e.preventDefault();
                          verifyOTPHandler();
                        }}
                      >
                        Verify OTP
                      </Button>
                    </CardFooter>
                  </Card>
                </Form>
              </Col>
            </Row>
          </Container>
          <div
            className="full-page-background"
            style={{
              backgroundImage: `url(${require("assets/img/bg/fabio-mangione.jpg")})`,
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default VerifyOTP;
