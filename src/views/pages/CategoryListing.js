import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

const CategoryListing = () => {
  const [categoryListingData, setCategoryListingData] = useState();
  const getCategoryList = async () => {
    const accessToken = Cookies.get("accessToken");
    const res = await axios.get(
      "http://16.171.173.51:3000/api/v1/admin/categories",
      {
        headers: {
          Authorization: accessToken,
        },
      }
    );
    setCategoryListingData(res?.data?.data);
  };

  useEffect(() => {
    getCategoryList();
  }, []);

  const dateFormatter = (d) => {
    const date = new Date(d).toLocaleDateString()
    return date
  }

  return (
    <div className="content">
      <Row>
        <Col md="12">
          <Card>
            <CardHeader>
              <CardTitle tag="h4">Category Listing</CardTitle>
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Category Title</th>
                    <th>Is Deleted</th>
                    <th>Created At</th>
                  </tr>
                </thead>
                <tbody>
                  {categoryListingData?.map((data) => {
                    return (
                      <tr>
                        <td>{data?.title}</td>
                        <td>{data?.isDeleted?"deleted":"not deleted"}</td>
                        <td>{dateFormatter(data?.createdAt)}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default CategoryListing;
