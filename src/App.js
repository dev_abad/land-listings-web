import React from "react";

import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";

import AuthLayout from "./layouts/Auth";
import AdminLayout from "./layouts/Admin";
import Login from "views/pages/Login";
import VerifyOTP from "views/pages/VerifyOtp";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/auth/*" element={<AuthLayout />} />
        <Route path="/admin/*" element={<AdminLayout />} />
        <Route path="/auth/login" element={<Login/>}/>
        <Route path="auth/verify-otp" element={<VerifyOTP/>}/>
        <Route path="*" element={<Navigate to="/auth/login" replace />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
